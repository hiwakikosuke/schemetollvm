#ifndef PROT_HPP
#define PROT_HPP

void test();
std::vector<Token> lexer(FILE* fp);
void initCharType(void);
Token getToken(FILE* fp);
int nextChar(FILE* fp);
Token setTokenType(Token tkn);
char *convertTokenType(TokenType tp);

#endif