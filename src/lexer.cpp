#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctype.h>
#include <vector>

#include "token.hpp"
#include "prot.hpp"

TokenType ctyp[256];    // character's tokentype
Token token;

struct {
    char *ktext;
    TokenType ktokenType;
} KwdTable[] = {
    { (char*)"="     ,  IDENTIFIER_TOKEN   },
    { (char*)"\'"    ,  S_QUOTES_TOKEN }, { (char*)"\""  ,   D_QUOTES   },
    { (char*)"`"     ,  B_QUOTES_TOKEN },
    { (char*)":"     ,  IDENTIFIER_TOKEN     }, { (char*)";"   ,   DELIMITER  },
    { (char*)","     ,  COMMA_TOKEN    }, { (char*)"."   ,   DOT_TOKEN        },
    { (char*)"("     ,  LPAREN_TOKEN   }, { (char*)")"   ,   RPAREN_TOKEN     },
    { (char*)"["     ,  LSQUARE  }, { (char*)"]"   ,   RSQUARE    },
    { (char*)"{"     ,  LBRACE   }, { (char*)"}"   ,   RBRACE     },
    { (char*)"+"     ,  IDENTIFIER_TOKEN     }, { (char*)"-"   ,   IDENTIFIER_TOKEN       },
    { (char*)"*"     ,  IDENTIFIER_TOKEN     }, { (char*)"/"   ,   IDENTIFIER_TOKEN       },
    { (char*)"%"     ,  IDENTIFIER_TOKEN     },
    { (char*)"<"     ,  IDENTIFIER_TOKEN     }, { (char*)">"   ,   IDENTIFIER_TOKEN       },
    { (char*)"#("    ,  HASH_LPAREN_TOKEN}, { (char*)""      ,  END_LIST },
};

// int main(int argc, char* argv[])
// {
//     if (argv[1] == NULL) { exit(1); }
//     else {
//         fp = fopen(argv[1], "r");
//         std::vector<Token> tokens = lexer(fp);
//     }
// }

std::vector<Token> lexer(FILE* fp)
{
    std::vector<Token> tokens;

    initCharType();

    // int i = 0;

    while (token.tokenType != E_O_T) {
        token = getToken(fp);
        tokens.push_back(token);
        // printf("%s:\n\t TokenType: %s\n", tokens[i].text, convertTokenType(tokens[i].tokenType));
        // i++;
    }

    fclose(fp);
    // printf("%s:\n\t TokenType: %s\n", tokens[0].text, convertTokenType(tokens[0].tokenType));
    return tokens;
}

void initCharType(void)
{
    int i;

    for (i =  0 ; i < 256; i++) { ctyp[i] = OTHERS; }
    for (i = '0'; i <= '9'; i++) { ctyp[i] = NUMBER_TOKEN; }
    for (i = 'a'; i <= 'z'; i++) { ctyp[i] = LETTER; }
    for (i = 'A'; i <= 'Z'; i++) { ctyp[i] = LETTER; }
    ctyp['(']  = LPAREN_TOKEN          ;  ctyp[')'] = RPAREN_TOKEN          ;
    ctyp['\''] = S_QUOTES_TOKEN        ;  ctyp['`'] = B_QUOTES_TOKEN        ;
    ctyp[',']  = COMMA_TOKEN           ;  ctyp['.'] = DOT_TOKEN             ;
    ctyp['"']  = D_QUOTES              ;  ctyp['#'] = HASH                  ;
    ctyp['+']  = IDENTIFIER_TOKEN      ;  ctyp['-'] = IDENTIFIER_TOKEN      ;
    ctyp['*']  = IDENTIFIER_TOKEN      ;  ctyp['/'] = IDENTIFIER_TOKEN      ;
    ctyp['%']  = IDENTIFIER_TOKEN      ;
    ctyp[';']  = SEMICOLON             ;
    ctyp['<']  = IDENTIFIER_TOKEN      ;  ctyp['>'] = IDENTIFIER_TOKEN      ;
    ctyp['!']  = IDENTIFIER_TOKEN      ;  ctyp['$'] = IDENTIFIER_TOKEN      ;
    ctyp[':']  = IDENTIFIER_TOKEN      ;  ctyp['?'] = IDENTIFIER_TOKEN      ;
    ctyp['=']  = IDENTIFIER_TOKEN      ;  ctyp['^'] = IDENTIFIER_TOKEN      ;
    ctyp['_']  = IDENTIFIER_TOKEN      ;  ctyp['~'] = IDENTIFIER_TOKEN      ;
}

Token getToken(FILE* fp)
{
    Token tkn = {NULLTYPE, ""};
    char *p = tkn.text, *p_31 = p+IDENT_SIZE, *p_100 = p+TEXT_SIZE;
    static int ch = ' ';

    while (isspace(ch)) { ch = nextChar(fp); }
    if (ch == EOF) { tkn.tokenType = E_O_T; return tkn; }

    switch (ctyp[ch]) {
        case LETTER:
            for ( ; ctyp[ch] == LETTER || ctyp[ch] == NUMBER_TOKEN; ch = nextChar(fp)) {
                if (p < p_31) *p++ = ch;
                *p = '\0';
            }
            break;
        case NUMBER_TOKEN:
            for ( ; ctyp[ch] == NUMBER_TOKEN || ctyp[ch] == DOT_TOKEN || ctyp[ch] == IDENTIFIER_TOKEN || ctyp[ch] == LETTER; ch = nextChar(fp)) {
                *p++ = ch;
                *p = '\0';
            }
            break;
        case S_QUOTES_TOKEN:
            *p++ = ch;
            *p++ = '\0';
            tkn.tokenType = S_QUOTES_TOKEN;
            ch = nextChar(fp);
            break;
        case D_QUOTES:
            *p++ = ch;
            for (ch = nextChar(fp); ch != '\"'; ch = nextChar(fp)) {
                if (p >= p_100) { printf("String literal is too long!\n"); exit(1); }
                else *p++ = ch;
            }
            *p++ = ch;
            *p = '\0';
            tkn.tokenType = STRING_TOKEN;
            ch = nextChar(fp);
            break;
        case COMMA_TOKEN:
            *p++ = ch;
            ch = nextChar(fp);
            if (p >= p_100) { printf("String literal is too long!\n"); exit(1); }
            else if(ch == '@')
            {
                *p++ = ch;
                *p = '\0';
                tkn.tokenType = COMMA_AT_TOKEN;
                ch = nextChar(fp);
                break;
            }
            else
            {
                *p = '\0';
                tkn.tokenType = COMMA_TOKEN;
                break;
            }
        case HASH:
            *p++ = ch;
            for (ch = nextChar(fp); ch != EOF && ch != '\n' && ch != ' '; ch = nextChar(fp)) {
                if (p >= p_100) { printf("Letter literal is too long!\n"); exit(1); }
                else if(ch == '\\')
                {
                    *p++ = ch;
                    for (ch = nextChar(fp); ch != EOF && ch != '\n' && ch != ' '; ch = nextChar(fp))
                    {
                        *p++ = ch;
                        tkn.tokenType = CHARACTER_TOKEN;
                        *p = '\0';
                    }
                    break;
                }
                else if(ch == 't' || ch == 'f')
                {
                    *p++ = ch;
                    tkn.tokenType = BOOLEAN_TOKEN;
                    *p = '\0';
                    ch = nextChar(fp);
                    break;
                }
                else if(ch == '(')
                {
                    tkn.tokenType = HASH_LPAREN_TOKEN;
                    *p = '\0';
                    break;
                }
            }
            break;
        case SEMICOLON:
            *p++ = ch;
            for (ch = nextChar(fp); ch != EOF && ch != '\n'; ch = nextChar(fp)) {
                *p++ = ch;
            }
            *p = '\0';
            tkn.tokenType = SEMICOLON;
            ch = nextChar(fp);
            break;
        default:
            *p++ = ch;
            ch = nextChar(fp);
            *p = '\0';
    }

    tkn = setTokenType(tkn);

    return tkn;
}

int nextChar(FILE* fp)
{
    static int c = 0;

    if (c == EOF) return c;

    c = fgetc(fp);
    if (c == EOF) return c;

    return c;
}

Token setTokenType(Token t)
{
    char* s = t.text;

    for (int i=0; KwdTable[i].ktokenType != END_LIST; i++) {
        if (strcmp(KwdTable[i].ktext, s) == 0) {
            t.tokenType = KwdTable[i].ktokenType;
            return t;
        }
    }

    if (t.tokenType != STRING_TOKEN && t.tokenType != BOOLEAN_TOKEN && t.tokenType != CHARACTER_TOKEN) {
        if (ctyp[(wint_t)*s] == LETTER) {
            t.tokenType = IDENTIFIER_TOKEN;
        }
        else if (ctyp[(wint_t)*s] == IDENTIFIER_TOKEN) {
            t.tokenType = IDENTIFIER_TOKEN;
        }
        else if (ctyp[(wint_t)*s] == NUMBER_TOKEN) {
            t.tokenType = NUMBER_TOKEN;
        }
        else if (ctyp[(wint_t)*s] == S_QUOTES_TOKEN) {
            t.tokenType = S_QUOTES_TOKEN;
        }
        else if (ctyp[(wint_t)*s] == D_QUOTES) {
            t.tokenType = D_QUOTES;
        }
    }

    return t;
}

char *convertTokenType(TokenType tp)
{
    switch (tp) {
        case IDENTIFIER_TOKEN:      return (char*)"INDENTIFIER"; break;
        case BOOLEAN_TOKEN:         return (char*)"BOOLEAN";     break;
        case NUMBER_TOKEN:          return (char*)"NUMBER";      break;
        case CHARACTER_TOKEN:       return (char*)"CHARACTER";   break;
        case STRING_TOKEN:          return (char*)"STRING";      break;
        case LPAREN_TOKEN:          return (char*)"LAPAREN";     break;
        case HASH_LPAREN_TOKEN:     return (char*)"HASH_LPAREN"; break;
        case RPAREN_TOKEN:          return (char*)"RPAREN";      break;
        case S_QUOTES_TOKEN:        return (char*)"S_QUOTES";    break;
        case B_QUOTES_TOKEN:        return (char*)"B_QUOTES";    break;
        case COMMA_TOKEN:           return (char*)"COMMA";       break;
        case COMMA_AT_TOKEN:        return (char*)"COMMA_AT";    break;
        case DOT_TOKEN:             return (char*)"DOT";         break;
        case SEMICOLON:             return (char*)"COMMENT";     break;
        default:        return NULL;
    }
}